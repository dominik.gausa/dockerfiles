#!/bin/bash

IMAGE_NAME="gausa/che_gcc_clangd:latest"

docker build -t${IMAGE_NAME} .

docker run --rm -it \
    ${IMAGE_NAME} \
    /bin/sh -c "git clone --depth=1 https://gitlab.com/dominik.gausa/cmake_hello.git && cd cmake_hello && cmake . && make all run"

if [ $? -eq 0 ]
then
    docker push ${IMAGE_NAME}
fi
